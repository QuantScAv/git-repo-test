#!/bin/bash
# v1.1
declare INPUT_FILE="cbaCallControl.ccxml"
declare -a LINES=()
declare COUNTER=0
declare -r TMP_FILE=$(mktemp -p /tmp tmp.XXXXXXXX)
trap '{ rm -f "$TMP_FILE"; }' EXIT SIGKILL

#------STYLE------

declare -r COLUMN_SPACES_AFTER=4
declare -r COLUMN_MAX_WIDTH=50
declare -r COLUMN_MIN_WIDTH=10

#------ARRAYS------

declare -a LAST_COLUMNS_MAP=()
declare -A LAST_COLUMNS_MAP_1=()

#------COMMON FUNCTIONS------

draw_field() {
    [[ $# -ge "2" ]] || return 1
    local align=${3}
    if [[ ${#align} == 0 ]]; then align='--right'; fi
    local length="$1"
    local f
    case "$align" in
        --right)
            f='%*s'
            printf $f $length "$2"
            ;;
        --left)
            f='%-*s'
            printf $f $length "$2"
            ;;
        --center)
            f='%*s'
            local col=$(( ($length + ${#2}) / 2))
            printf $f $col "$2"
            printf $f $(( $length - $col ))
            ;;
        *)  ;;
    esac
    return 0
}

draw_hline() {
    local c=${1}
    local l=${2}
    local line
    printf -v line '%*s' $l
    line=${line// /$c}
    echo "$line"
}

draw_table_head() {
    local result col line
    local counter=0
    LAST_COLUMNS_MAP=()
    for col; do
        (( ${#col} > $COLUMN_MIN_WIDTH )) && LAST_COLUMNS_MAP+=($COLUMN_MAX_WIDTH) || LAST_COLUMNS_MAP+=($COLUMN_MIN_WIDTH)
        printf -v line "%s%s" "$(draw_hline ' ' $COLUMN_SPACES_AFTER)" "$(draw_field ${LAST_COLUMNS_MAP[$counter]:-${#col}} "$col" --center)"
        result="${result}${line}"
        : $((counter++))
    done
    result="${result}\n" # --- new line ---
    for col in ${LAST_COLUMNS_MAP[@]}; do
        printf -v line "%s" "$(draw_hline ' ' $COLUMN_SPACES_AFTER)" "$(draw_hline '-' $col)"
        result="${result}${line}"
    done
    echo -e "$result"
}

draw_lines() {
    local result col line counter=0
    for col in ${LINES[@]}; do
        (( ${#col} > $COLUMN_MIN_WIDTH )) && LAST_COLUMNS_MAP+=($COLUMN_MAX_WIDTH) || LAST_COLUMNS_MAP+=($COLUMN_MIN_WIDTH)
        printf -v line "%s%s" "$(draw_hline ' ' $COLUMN_SPACES_AFTER)" "$(draw_field ${LAST_COLUMNS_MAP[$counter]:-${#col}} "$col" --left)"
        result="${result}${line}"
        : $((counter++))
    done
    echo -e "$result"
    LINES=()
}

parse_transition_element() {
    local state='' event='' line=''
    [[ -n $1 ]] || { echo "WARN: Empty statement"; return 1; }
    line=${1//[[:space:]]/&}
    state=$(echo -n "$line" | grep -oE "state=\"[^ ]+\"" | grep -oE '[^=]*"')
    event=$(echo -n "$line" | grep -oE "event=\"[^ ]+\"" | grep -oE '[^=]*"')
    [[ -n $state ]] || state='N/A'
    [[ -n $event ]] || event='N/A'
    echo -n "$state $event"
}

parse_send_element() {
    local name
    [[ -n $1 ]] || { echo "WARN: Empty statement"; return 1; }
    name=$(echo -n "$1" | grep -oE "name=\"[^ ]+\"" | grep -oE '[^=]*"')
    echo -n "$name"
}

split_by_cols() {
    local arg counter=0
    for arg; do
        LINES[$counter]=$arg
        : $((counter++))
    done
}

#------PROCEDURES------

echo "File             : $INPUT_FILE"
echo "Last modification: $(stat -c "%y" "$INPUT_FILE")"

print_transitions() {
    echo "===================="
    echo "Index of transitions"
    echo "===================="
    draw_table_head "Line No." "     State     " "     Event     "
    sed -n -e '/<transition \<[^>]*>/ {
        =;p
    }
    ' "$INPUT_FILE" | while read -r p1 p2; do
        if [[ $p1 =~ \<transition ]]; then
            LINES=(${LINES[*]} $(parse_transition_element "$p2"))
            ((${#LINES[@]} >= 4)) && LINES[${#LINES[@]}-1]=""
            echo "${LINES[*]} <=transition" >> $TMP_FILE
            draw_lines
        else
            LINES+=("$p1")
        fi
    done
}
print_transitions

print_dialogprepare() {
    local number dialogid src app counter=0
    echo "======================"
    echo "Index of dialogprepare"
    echo "======================"
    #draw_table_head "Line No." "     Dialog ID     " "     Source     " "     Application     "
    sed -n -e '/<dialogprepare/ {
        =
    }
    ' \
    -e '/<dialogprepare/,/\/>/ {
        p
    }
    ' "$INPUT_FILE" | while IFS= read -r; do
        echo "${REPLY//[[:space:]]/ }"
    done
}
print_dialogprepare

print_send() {
    echo "======================"
    echo "  Index of sendings   "
    echo "======================"
    draw_table_head "Line No." "     Name     "
    sed -n -e '/<send \<[^>]*>/ {
        =;p
    }
    ' "$INPUT_FILE" | while read -r p1 p2; do
        if [[ $p1 =~ \<send ]]; then
            LINES=(${LINES[*]} $(parse_send_element "$p2"))
            echo "${LINES[*]} <=send_event" >> $TMP_FILE
            draw_lines
        else
            LINES+=("$p1")
        fi
    done

}
print_send

print_full_map() {
    local key line result first_col
    echo "======================"
    echo "       Full Map       "
    echo "======================"
    while read -r line || [[ -n $line ]]; do
        LINES=()
        split_by_cols $line
        first_col=${LINES[0]}
        LINES[0]=""
        if (( ${#LINES[@]} == 3 )); then
            LINES[3]=${LINES[2]}
            LINES[2]="---//---"
        fi
        LAST_COLUMNS_MAP_1[$first_col]=${LINES[*]}
    done < "$TMP_FILE"
    draw_table_head "Line No." "     Column 1     " "     Column 2     " "Column 3"
    LINES=()
    for key in "${!LAST_COLUMNS_MAP_1[@]}"; do
        LINES=($key ${LAST_COLUMNS_MAP_1[$key]})
        draw_lines
    done | sort -n -k1
}
print_full_map
